from setuptools import setup
import os

if not os.path.isfile("README.rst"):
    raise RuntimeError("Generate README.rst from README.md before running tox or setup")

long_description = None
with open('./README.rst') as fd:
    long_description = fd.read()

setup(
    name='funcipy',
    packages=['funcipy'],
    version='0.5',
    description='A library to inject common functional programming operations as methods into iterable objects.',
    long_description=long_description,
    author='Venkatesh-Prasad Ranganath',
    license='BSD 3-Clause License',
    platforms=['unix', 'linux', 'osx', 'cygwin', 'win32'],
    keywords=['functional', 'programming'],
    install_requires=[],
    download_url='https://bitbucket.org/rvprasad/funcipy/downloads/?tab=tags',
    url='https://bitbucket.org/rvprasad/funcipy',
    classifiers=[
        'Intended Audience :: Developers',
        'Topic :: Software Development :: Libraries',
        'Development Status :: 5 - Production/Stable',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
    ],
)
