# Copyright (c) 2017-18, Kansas State University
#
# BSD 3-clause License
#
# Author: Venkatesh-Prasad Ranganath (rvprasad)

from .funcipy import funcify
