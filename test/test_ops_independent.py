# Copyright (c) 2017, Venkatesh-Prasad Ranganath
#
# BSD 3-clause License
#
# Author: Venkatesh-Prasad Ranganath (rvprasad)

from funcipy import funcify


def test_funcify_is_identity_for_non_iterables():
    class CustomClass(object):
        pass

    for i in [1, 3.4, True, None, CustomClass()]:
        assert i is funcify(i)
