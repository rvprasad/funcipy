# Copyright (c) 2017, Venkatesh-Prasad Ranganath
#
# BSD 3-clause License
#
# Author: Venkatesh-Prasad Ranganath (rvprasad)

from .helpers import *
from funcipy import funcify
from hypothesis import assume, example, given, strategies
import functools
import operator
import pytest


class BaseTestFold:

    def non_empty_helper(self, obj, op):
        assert False

    def empty_helper(self, funced_obj, op):
        assert False

    @given(strategies.lists(strategies.text(), min_size=1, max_size=20))
    def test_fold_non_empty_list(self, obj):
        print(type(self))
        op = operator.concat
        m1, m2, m3, funced_obj = self.non_empty_helper(obj, op)

        assert funced_obj is not obj
        assert_helper_value(m1, m2, m3)

    @given(strategies.sets(strategies.integers(), min_size=1, max_size=20))
    def test_fold_non_empty_set(self, obj):
        op = operator.add
        m1, m2, m3, funced_obj = self.non_empty_helper(obj, op)

        assert funced_obj is not obj
        assert_helper_value(m1, m2, m3)

    @given(strategies.dictionaries(strategies.integers(),
                                   strategies.integers(),
                                   min_size=1, max_size=20))
    def test_fold_non_empty_dict(self, obj):
        op = operator.mul
        m1, m2, m3, funced_obj = self.non_empty_helper(obj, op)

        assert funced_obj is not obj
        assert_helper_value(m1, m2, m3)

    @given(strategies.tuples(strategies.integers(), strategies.integers(),
                             strategies.integers()))
    def test_fold_non_empty_tuple(self, obj):
        op = operator.xor
        m1, m2, m3, funced_obj = self.non_empty_helper(obj, op)

        assert funced_obj is not obj
        assert_helper_value(m1, m2, m3)

    @given(strategies.text(min_size=1, max_size=20))
    def test_fold_non_empty_string(self, obj):
        op = min
        m1, m2, m3, funced_obj = self.non_empty_helper(obj, op)

        assert funced_obj is not obj
        assert_helper_value(m1, m2, m3)

    @given(strategies.integers(min_value=-1000, max_value=1000),
           strategies.integers(min_value=-1000, max_value=1000))
    def test_fold_non_empty_range(self, mn, size):
        assume(size != 0)
        step = 1 if size > 0 else -1
        obj = range(mn, mn + size, step)
        op = operator.or_
        m1, m2, m3, funced_obj = self.non_empty_helper(obj, op)

        assert funced_obj is not obj
        assert_helper_value(m1, m2, m3)

    @given(strategies.one_of(strategies.lists(strategies.integers(),
                                              min_size=1),
                             strategies.sets(strategies.integers(),
                                             min_size=1),
                             strategies.tuples(strategies.integers(),
                                               strategies.integers()),
                             strategies.dictionaries(strategies.integers(),
                                                     strategies.booleans(),
                                                     min_size=1)))
    def test_fold_non_empty_iterables(self, obj):
        assume(len(obj) > 0)
        op = operator.mul
        m1, m2, m3, funced_obj = self.non_empty_helper(obj, op)

        assert_helper_value(m1, m2, m3)

    def test_fold_non_empty_custom_iterable(self):
        obj = CustomContainer(100)
        op = max
        m1, m2, m3, funced_obj = self.non_empty_helper(obj, op)

        assert funced_obj is obj
        assert_helper_value(m1, m2, m3)

    @given(strategies.iterables(min_size=0, max_size=0))
    @example(CustomContainer(1))
    def test_fold_empty_iterables(self, iterable):
        with pytest.raises(TypeError):
            next(self.empty_helper(funcify(iterable), str))


class TestFoldL(BaseTestFold):

    def non_empty_helper(self, obj, op):
        m1 = functools.reduce(op, obj)
        funced_obj = funcify(obj)
        m2 = funced_obj.foldl(op)
        m3 = functools.reduce(op, funced_obj)
        return m1, m2, m3, funced_obj

    def empty_helper(self, funced_obj, op):
        return funced_obj.foldl(op)


class TestFoldR(BaseTestFold):

    def non_empty_helper(self, obj, op):
        m1 = functools.reduce(op, reversed(list(obj)))
        funced_obj = funcify(obj)
        m2 = funced_obj.foldr(op)
        m3 = functools.reduce(op, reversed(list(funced_obj)))
        return m1, m2, m3, funced_obj

    def empty_helper(self, funced_obj, op):
        return funced_obj.foldr(op)
