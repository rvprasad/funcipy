# Copyright (c) 2017, Venkatesh-Prasad Ranganath
#
# BSD 3-clause License
#
# Author: Venkatesh-Prasad Ranganath (rvprasad)

from .helpers import *
from funcipy import funcify
from hypothesis import assume, example, given, strategies
import functools
import operator
import pytest


def helper(obj, op):
    m1 = map(op, obj)
    funced_obj = funcify(obj)
    m2 = funced_obj.map(op)
    m3 = map(op, funced_obj)
    return m1, m2, m3, funced_obj


@given(strategies.lists(strategies.integers(), min_size=1))
def test_map_non_empty_list(obj):
    op = operator.abs
    m1, m2, m3, funced_obj = helper(obj, op)

    assert funced_obj is not obj
    assert_helper_ordered_seq(m1, m2, m3)


@given(strategies.sets(strategies.integers(), min_size=1))
def test_map_non_empty_set(obj):
    op = operator.inv
    m1, m2, m3, funced_obj = helper(obj, op)

    assert funced_obj is not obj
    assert_helper_unordered_seq(m1, m2, m3)


@given(strategies.dictionaries(strategies.text(), strategies.integers(),
                               min_size=1))
def test_map_non_empty_dict(obj):
    op = len
    m1, m2, m3, funced_obj = helper(obj, op)

    assert funced_obj is not obj
    assert_helper_unordered_seq(m1, m2, m3)


@given(strategies.tuples(strategies.integers(), strategies.integers(),
                         strategies.integers()))
def test_map_non_empty_tuple(obj):
    op = operator.truth
    m1, m2, m3, funced_obj = helper(obj, op)

    assert funced_obj is not obj
    assert_helper_ordered_seq(m1, m2, m3)


@given(strategies.text(min_size=1))
def test_map_non_empty_string(obj):
    op = functools.partial(operator.mul, 2)
    m1, m2, m3, funced_obj = helper(obj, op)

    assert funced_obj is not obj
    assert_helper_ordered_seq(m1, m2, m3)


@given(strategies.integers(min_value=-1000, max_value=1000),
       strategies.integers(min_value=-1000, max_value=1000))
def test_map_non_empty_range(mn, size):
    assume(size != 0)
    step = 1 if size > 0 else -1
    obj = range(mn, mn + size, step)
    op = operator.abs
    m1, m2, m3, funced_obj = helper(obj, op)

    assert funced_obj is not obj
    assert_helper_ordered_seq(m1, m2, m3)


@given(strategies.one_of(strategies.lists(strategies.integers(), min_size=1),
                         strategies.sets(strategies.integers(), min_size=1),
                         strategies.tuples(strategies.integers(),
                                           strategies.integers()),
                         strategies.dictionaries(strategies.integers(),
                                                 strategies.booleans(),
                                                 min_size=1)))
def test_map_non_empty_iterables(obj):
    op = operator.abs
    m1, m2, m3, funced_obj = helper(obj, op)

    assert funced_obj is not obj
    assert_helper_unordered_seq(m1, m2, m3)


def test_map_non_empty_custom_iterable():
    obj = CustomContainer(100)
    op = str
    m1, m2, m3, funced_obj = helper(obj, op)

    assert funced_obj is obj
    assert_helper_ordered_seq(m1, m2, m3)


@given(strategies.iterables(min_size=0, max_size=0))
@example(CustomContainer(1))
def test_map_empty_iterable(obj):
    with pytest.raises(StopIteration):
        next(funcify(obj).map(str))
