# Copyright (c) 2017, Venkatesh-Prasad Ranganath
#
# BSD 3-clause License
#
# Author: Venkatesh-Prasad Ranganath (rvprasad)


from funcipy import funcify
from hypothesis import given, strategies
import functools
import operator


@given(strategies.one_of(strategies.lists(strategies.booleans()),
                         strategies.sets(strategies.booleans()),
                         strategies.tuples(strategies.booleans(),
                                           strategies.booleans()),
                         strategies.dictionaries(strategies.booleans(),
                                                 strategies.booleans())))
def test_any(obj):
    tmp1 = funcify(obj)
    assert tmp1.any() == any(obj)
    assert any(tmp1) == any(obj)


@given(strategies.one_of(strategies.lists(strategies.booleans()),
                         strategies.sets(strategies.booleans()),
                         strategies.tuples(strategies.booleans(),
                                           strategies.booleans()),
                         strategies.dictionaries(strategies.booleans(),
                                                 strategies.booleans())))
def test_all(obj):
    tmp1 = funcify(obj)
    assert tmp1.all() == all(obj)
    assert all(tmp1) == all(obj)


@given(strategies.one_of(strategies.lists(strategies.integers()),
                         strategies.sets(strategies.integers()),
                         strategies.tuples(strategies.integers(),
                                           strategies.integers()),
                         strategies.dictionaries(strategies.integers(),
                                                 strategies.booleans())))
def test_sum(obj):
    tmp1 = funcify(obj)
    assert tmp1.sum() == sum(obj)
    assert sum(tmp1) == sum(obj)


@given(strategies.one_of(strategies.lists(strategies.integers(), min_size=1),
                         strategies.sets(strategies.integers(), min_size=1),
                         strategies.tuples(strategies.integers(),
                                           strategies.integers()),
                         strategies.dictionaries(strategies.integers(),
                                                 strategies.booleans(),
                                                 min_size=1)))
def test_max(obj):
    tmp1 = funcify(obj)
    assert tmp1.max() == max(obj)
    assert max(tmp1) == max(obj)


@given(strategies.one_of(strategies.lists(strategies.integers(), min_size=1),
                         strategies.sets(strategies.integers(), min_size=1),
                         strategies.tuples(strategies.integers(),
                                           strategies.integers()),
                         strategies.dictionaries(strategies.integers(),
                                                 strategies.booleans(),
                                                 min_size=1)))
def test_min(obj):
    tmp1 = funcify(obj)
    assert tmp1.min() == min(obj)
    assert min(tmp1) == min(obj)


@given(strategies.one_of(strategies.lists(strategies.integers()),
                         strategies.sets(strategies.integers()),
                         strategies.tuples(strategies.integers(),
                                           strategies.integers()),
                         strategies.dictionaries(strategies.integers(),
                                                 strategies.booleans())),
       strategies.integers())
def test_count_ints(obj, k):
    tmp1 = funcify(obj)
    pred = functools.partial(operator.lt, k)
    assert len(list(filter(pred, obj))) == tmp1.count(pred)
    assert len(list(filter(pred, obj))) == len(list(filter(pred, tmp1)))


@given(strategies.one_of(strategies.lists(strategies.text()),
                         strategies.sets(strategies.text()),
                         strategies.tuples(strategies.text(),
                                           strategies.text()),
                         strategies.dictionaries(strategies.text(),
                                                 strategies.booleans())),
       strategies.integers())
def test_count_strings(obj, k):
    tmp1 = funcify(obj)
    pred = lambda x: len(x) > k
    assert len(list(filter(pred, obj))) == tmp1.count(pred)
    assert len(list(filter(pred, obj))) == len(list(filter(pred, tmp1)))
