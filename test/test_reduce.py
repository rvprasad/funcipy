# Copyright (c) 2017, Venkatesh-Prasad Ranganath
#
# BSD 3-clause License
#
# Author: Venkatesh-Prasad Ranganath (rvprasad)

from .helpers import *
from funcipy import funcify
from hypothesis import example, given, strategies
import functools
import operator
import pytest


def helper(obj, op, init_val):
    m1 = functools.reduce(op, obj, init_val)
    funced_obj = funcify(obj)
    m2 = funced_obj.reduce(op, init_val)
    m3 = functools.reduce(op, funced_obj, init_val)
    return m1, m2, m3, funced_obj


@given(strategies.lists(strategies.text(), min_size=1, max_size=20),
       strategies.text())
def test_reduce_list(obj, init_val):
    op = operator.concat
    m1, m2, m3, funced_obj = helper(obj, op, init_val)

    assert funced_obj is not obj
    assert_helper_value(m1, m2, m3)


@given(strategies.sets(strategies.integers(), min_size=1, max_size=20),
       strategies.integers())
def test_reduce_set(obj, init_val):
    op = operator.add
    m1, m2, m3, funced_obj = helper(obj, op, init_val)

    assert funced_obj is not obj
    assert_helper_value(m1, m2, m3)


@given(strategies.dictionaries(strategies.integers(), strategies.integers(),
                               min_size=1, max_size=20),
       strategies.integers())
def test_reduce_dict(obj, init_val):
    op = operator.mul
    m1, m2, m3, funced_obj = helper(obj, op, init_val)

    assert funced_obj is not obj
    assert_helper_value(m1, m2, m3)


@given(strategies.tuples(strategies.integers(), strategies.integers(),
                         strategies.integers()),
       strategies.integers())
def test_reduce_tuple(obj, init_val):
    op = operator.xor
    m1, m2, m3, funced_obj = helper(obj, op, init_val)

    assert funced_obj is not obj
    assert_helper_value(m1, m2, m3)


@given(strategies.text(min_size=1, max_size=20), strategies.text())
def test_reduce_string(obj, init_val):
    op = min
    m1, m2, m3, funced_obj = helper(obj, op, init_val)

    assert funced_obj is not obj
    assert_helper_value(m1, m2, m3)


@given(strategies.integers(min_value=-1000, max_value=1000),
       strategies.integers(min_value=-1000, max_value=1000),
       strategies.integers())
def test_reduce_range(mn, size, init_val):
    step = 1 if size > 0 else -1
    obj = range(mn, mn + size, step)
    op = operator.or_
    m1, m2, m3, funced_obj = helper(obj, op, init_val)

    assert funced_obj is not obj
    assert_helper_value(m1, m2, m3)


@given(strategies.one_of(strategies.lists(strategies.integers()),
                         strategies.sets(strategies.integers()),
                         strategies.tuples(strategies.integers()),
                         strategies.dictionaries(strategies.integers(),
                                                 strategies.booleans())),
       strategies.integers())
def test_reduce_iterables(obj, init_val):
    op = operator.mul
    m1, m2, m3, funced_obj = helper(obj, op, init_val)

    assert_helper_value(m1, m2, m3)


@given(strategies.integers())
def test_reduce_custom_iterable_class(init_val):
    obj = CustomContainer(100)
    op = max
    m1, m2, m3, funced_obj = helper(obj, op, init_val)

    assert funced_obj is obj
    assert_helper_value(m1, m2, m3)


@given(strategies.iterables(min_size=0, max_size=0))
@example(CustomContainer(0))
def test_reduce_empty_iterable_without_init_val(iterable):
    with pytest.raises(TypeError):
        funcify(iterable).reduce(operator.add)


@given(strategies.iterables(min_size=0, max_size=0), strategies.integers())
def test_reduce_empty_list_with_init_val(iterable, init_val):
    assert funcify(iterable).reduce(operator.add, init_val) == init_val
