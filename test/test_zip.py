# Copyright (c) 2017, Venkatesh-Prasad Ranganath
#
# BSD 3-clause License
#
# Author: Venkatesh-Prasad Ranganath (rvprasad)

from .helpers import *
from funcipy import funcify
from hypothesis import given, strategies


@given(strategies.one_of(strategies.lists(strategies.integers()),
                         strategies.sets(strategies.integers()),
                         strategies.dictionaries(strategies.integers(),
                                                 strategies.booleans()),
                         strategies.tuples(strategies.integers(),
                                           strategies.integers())),
      strategies.one_of(strategies.lists(strategies.integers()),
                        strategies.sets(strategies.integers()),
                        strategies.dictionaries(strategies.integers(),
                                                strategies.booleans()),
                         strategies.tuples(strategies.integers(),
                                           strategies.integers())))
def test_zip(iter1, iter2):
    l1 = list(iter1)
    l2 = list(iter2)
    tmp1 = funcify(l1)
    m1 = zip(l1, l2)
    m2 = tmp1.zip(l2)
    m3 = zip(tmp1, l2)
    assert_helper_ordered_seq(m1, m2, m3)
