
# Copyright (c) 2017, Venkatesh-Prasad Ranganath
#
# BSD 3-clause License
#
# Author: Venkatesh-Prasad Ranganath (rvprasad)

from collections import Counter
import pytest


class CustomContainer(object):
    def __init__(self, k):
        self.r = range(1, k)
        self.i = iter(self.r)

    def __iter__(self):
        for i in self.r:
            yield i


def assert_helper_ordered_seq(m1, m2, m3):
    for tmp1 in m1:
        assert tmp1 == next(m2)
        assert tmp1 == next(m3)
    with pytest.raises(StopIteration):
        assert next(m1)
    with pytest.raises(StopIteration):
        assert next(m2)


def assert_helper_unordered_seq(m1, m2, m3):
    s1 = Counter(m1)
    assert s1 == Counter(m2)
    assert s1 == Counter(m3)


def assert_helper_value(v1, v2, v3):
    assert v1 == v2
    assert v1 == v3
